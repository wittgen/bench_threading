cmake_minimum_required(VERSION 3.26)
project(bench_threading)
include(cmake/CPM.cmake)
CPMAddPackage("gh:catchorg/Catch2@3.2.1")
set(CMAKE_CXX_STANDARD 20)

add_executable(bench_threading main.cpp)
add_executable(worker worker.cpp)