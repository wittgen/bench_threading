#include <iostream>
#include <array>

template<typename T, size_t Size>
struct Bucket_ {
    size_t seq_{};
    size_t len_{};
    std::array<T, Size> data;
};

template<typename Type, size_t Size>
class RingBuffer_ {
public:
    RingBuffer_() = default;

private:
    std::array<Type, Size> buffer_;
};

using Bucket = Bucket_<uint64_t, 4096>;
using RingBuffer = RingBuffer_<Bucket, 512>;
// pingPongAtomicFlag.cpp

#include <iostream>
#include <atomic>
#include <thread>

constexpr size_t nThreads = 16;
size_t run_count[nThreads] = {0};
std::atomic_flag condAtomicFlag[nThreads]{false, false, false, false};
std::atomic<int> counter = {0};
std::atomic_flag end{false};
constexpr int countlimit = 10'000'000;
int dropped = 0;

void ping() {
    static int last = -1;
    while (counter < countlimit) {
        last++;
        int i = 0;
        int loop = (last + nThreads);
        for (i = last;; ++i) {
            if (!condAtomicFlag[i % nThreads].test()) {
                condAtomicFlag[i % nThreads].test_and_set();
                condAtomicFlag[i % nThreads].notify_one();
                last = i;
                break;
            }
        }
        //if (i == loop) dropped++;
        ++counter;
    }
    for (auto &a: condAtomicFlag) {
        a.test_and_set();
        a.notify_one();
    }
}

void pong(int i) {
    while (counter < countlimit) {
        condAtomicFlag[i].wait(false);
        run_count[i]++;
        condAtomicFlag[i].clear();
        condAtomicFlag[i].notify_one();
    }
}

int main() {

    auto start = std::chrono::system_clock::now();
    std::thread consumers[nThreads];

    for (int i = 0; auto &c: consumers)
        c = std::thread(pong, i++);
    std::thread t1(ping);

    for (auto &c: consumers)
        c.join();
    t1.join();

    std::chrono::duration<double> dur = std::chrono::system_clock::now() - start;
    std::cout << "Duration: " << dur.count() << " seconds" << std::endl;
    size_t sum{0};
    for (auto &i: run_count) {
        std::cout << i << "\n";
        sum += i;
    }
    std::cout << "Sum:     " << sum << "\n";
    std::cout << "Counter: " << counter << "\n";
    std::cout << "Dropped: " << dropped << "\n";
}

