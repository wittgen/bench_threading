#include <iostream>
#include <array>

template<typename T, size_t Size>
struct Bucket_ {
    size_t seq_{};
    size_t len_{};
    std::array<T, Size> data;
};

template<typename Type, size_t Size>
class RingBuffer_ {
public:
    RingBuffer_() = default;

private:
    std::array<Type, Size> buffer_;
};

using Bucket = Bucket_<uint64_t, 4096>;
using RingBuffer = RingBuffer_<Bucket, 512>;
// pingPongAtomicFlag.cpp

#include <iostream>
#include <atomic>
#include <thread>
std::atomic<int> run = 1;
constexpr size_t nThreads = 4;
size_t run_count[nThreads] = {0};
std::atomic_flag condAtomicFlag[nThreads]{false};
std::atomic<int> counter = {0};
std::atomic_flag end{false};
constexpr int countlimit = 10'000'000;


void ping() {
    while (counter<countlimit) {
        for(unsigned i =0 ; i<nThreads;i++) {
            condAtomicFlag[i].test_and_set();
            condAtomicFlag[i].notify_one();
        }
        //std::cout << ": " << done<< std::endl;
        for(unsigned i =0 ; i<nThreads;i++) {
            condAtomicFlag[i].wait(true);
        }

        //std::cout << "done ping \n";
        counter++;
    }
    run = 0;
    for(unsigned i =0 ; i<nThreads;i++) {
        condAtomicFlag[i].test_and_set();
        condAtomicFlag[i].notify_one();
    }

    //std::cout << "exit pong\n";
}

void pong(int i) {
    while (true) {
        condAtomicFlag[i].wait(false);
        if(!run) break;
        run_count[i]++;
        //std::cout  << counter << "\n";
        condAtomicFlag[i].clear();
        condAtomicFlag[i].notify_one();
    }
    //std::cout << "exit\n";
}

int main() {

    auto start = std::chrono::system_clock::now();
    std::thread consumers[nThreads];

    for (int i = 0; auto &c: consumers)
        c = std::thread(pong, i++);
    std::thread t1(ping);

    for (auto &c: consumers)
        c.join();
    t1.join();

    std::chrono::duration<double> dur = std::chrono::system_clock::now() - start;
    std::cout << "Duration: " << dur.count() << " seconds" << std::endl;
    size_t sum{0};
    for (auto &i: run_count) {
        std::cout << i << "\n";
        sum += i;
    }
    std::cout << "Sum:     " << sum << "\n";
    std::cout << "Counter: " << counter << "\n";
}

